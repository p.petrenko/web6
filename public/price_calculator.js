function definePrice() {

    let data = {
        opt1: {
            basePrice: 15000
        },
        opt2: {
            basePrice: 30000,
            radios_opts: {
                rad1: 1000,
                rad2: 2000,
                rad3: 3000
            }
        },
        opt3: {
            basePrice: 100000,
            checkbox_opt: 20000
        }
    };


    let r = document.getElementById("price_result");
    let input_area = document.getElementById("price_quantity");
    let select = document.getElementById("myselect");

    let radios = document.getElementsByName("myradio");
    let radio_sum = 0;
    if (radios[0].checked) {
        radio_sum = data.opt2.radios_opts.rad1;
    }
    if (radios[1].checked) {
        radio_sum = data.opt2.radios_opts.rad2;
    }
    if (radios[2].checked) {
        radio_sum = data.opt2.radios_opts.rad3;
    }

    let checkbox = document.getElementById("calculate_cb");
    let checkbox_sum = 0;
    if (checkbox.checked) {
        checkbox_sum = data.opt3.checkbox_opt;
    }

    if (select.value === "1") {
        r.innerHTML = input_area.value * data.opt1.basePrice;
    } else if (select.value === "2") {
        r.innerHTML = input_area.value * data.opt2.basePrice + radio_sum;

    } else if (select.value === "3") {
        r.innerHTML = input_area.value * data.opt3.basePrice + checkbox_sum;
    }
}

window.addEventListener("DOMContentLoaded", function(event) {
    let calc = document.getElementById("price_calculate");
    calc.addEventListener("change", function(event) {
        let r = document.getElementById("price_result");
        r.innerHTML = 0;
        let input_area = document.getElementById("price_quantity");
        let select = document.getElementById("myselect");
        let radio_block = document.getElementById("myradios");
        let checkbox_block = document.getElementById("calculate_cb_block");
        if (input_area.value === "") {
            radio_block.style.display = "none";
            checkbox_block.style.display = "none";
            select.value = "disabled selected value";
            r.innerHTML = 0;
        } else if (!(/^(0|[1-9][0-9]*)$/).test(input_area.value)) {
            alert("Введите корректное число заявок");
            input_area.value = "";
            radio_block.style.display = "none";
            checkbox_block.style.display = "none";
            select.value = "disabled selected value";
            r.innerHTML = 0;
        } else {
            if (select.value === "1") {
                radio_block.style.display = "none";
                checkbox_block.style.display = "none";

            } else if (select.value === "2") {
                radio_block.style.display = "block";
                checkbox_block.style.display = "none";
            } else if (select.value === "3") {
                radio_block.style.display = "none";
                checkbox_block.style.display = "block";
            }
            definePrice(select.value);
        }
    });
});